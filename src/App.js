import React  from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { observer } from "mobx-react-lite";
import Registration from './components/Registration';
import Login from './components/Login';
import Logout from './components/Logout';
import Subject from './components/Subject';
import Task from './components/Task';
import Post from './components/Post';
import Comment from './components/Comment';
import Header from './components/Header';
import UserProfile from './components/UserProfile';
import ExternUserProfile from './components/ExternUserProfile'
import ProtectedRoute from './components/ProtectedRoute';
import Feed from './components/Feed';
import FeedComment from './components/FeedComment';
import Tred from "./components/Tred";

const App = observer(() => {
  return (
    <Router>
      <div>
        {Header()}
        <Routes>
            <Route path="/registration" element={<Registration/>} />
            <Route path="/login" element={<Login/>} />/
            <Route path="/logout" element={<Logout />}/>
            
            <Route 
              path="/profile"
              element={
                <ProtectedRoute>
                  {UserProfile()}
                </ProtectedRoute>
            }/>
            <Route 
              path="/profile/:username"
              element={
                <ProtectedRoute>
                  <ExternUserProfile />
                </ProtectedRoute>
            }/>
            <Route
                path="/feed"
                element={
                    <ProtectedRoute>
                        <Feed/>
                    </ProtectedRoute>
            }/>
            <Route
              path="/"
              element={
                <ProtectedRoute>
                  <Subject/>
                </ProtectedRoute>
            }/>
            <Route 
              path="/tasks/:subjectId"
              element={
                <ProtectedRoute>
                  <Task />
                </ProtectedRoute>
            }/>
            <Route 
              path="/posts/:taskId"
              element={
                <ProtectedRoute>
                  <Post />
                </ProtectedRoute>
            }/>
            <Route 
              path="/comments/:postId"
              element={
                <ProtectedRoute>
                  <Comment />
                </ProtectedRoute>
            }/>
            <Route
                path="feed/comments/:postId"
                element={
                    <ProtectedRoute>
                        <FeedComment/>
                    </ProtectedRoute>
                }/>
            <Route
                path="feed/comments/:postId/:commentId"
                element={
                    <ProtectedRoute>
                        <Tred/>
                    </ProtectedRoute>
                }/>
        </Routes>
      </div>
    </Router>
  );
});

export default App;
