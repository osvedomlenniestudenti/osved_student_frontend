import axios from "axios";

const API_URL = "http://localhost:9999/auth";

class AuthService {
  login(name, password) {
    return axios
      .post(API_URL + "/login", {
        name,
        password
      })
      .then(response => {
        if (response.data.token) {
          localStorage.setItem("token", response.data.token);
        }
        return response.status;
      })
      .catch(response => {
         alert(response.response.data.message);
       
        return response.response.status;
      });
  }

  logout() {
    localStorage.removeItem("token");
  }

  register(name, password, email, age) {
    return axios
      .post(API_URL + "/register", {
        "name": name,
        "password": password,
        "email": email,
        "age": age
      })
      .then(response => {
        if (response.data.token) {
          localStorage.setItem("token", response.data.token);
        }
        return response.status;
      })
      .catch(response => {
          alert(response.response.data.message);
        return response.response.status;
      });
  }
}

export default new AuthService();