import { useState } from "react";
import AuthService from "../services/AuthService";
import { useNavigate } from "react-router-dom";

const Login = () => {
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [mail, setMail] = useState('');
    const [age, setAge] = useState('');

    const registerUser = async () => {
        AuthService.register(name, password, mail, age)
        .then(res => {
            if (res === 200) {
                navigate("/feed");
            }
        });
    };

    return (
        <div style={{width: "120px"}}>
            <p>Регистрация</p>
            <label>Введите имя</label><input type="text" value={name} onChange={(e) => setName(e.target.value)} required /><br/>
            <label>Введите пароль</label><input type="password" value={password} onChange={(e) => setPassword(e.target.value)} required /><br/>
            <label>Введите почту</label><input type="mail" value={mail} onChange={(e) => setMail(e.target.value)} required /><br/>
            <label>Введите возраста</label><input value={age} onChange={(e) => setAge(e.target.value)} required /><br/>
            <button onClick={registerUser}>Войти</button>
        </div>
    );
};

export default Login;