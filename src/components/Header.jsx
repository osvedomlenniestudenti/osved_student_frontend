import { Link, useParams } from 'react-router-dom';



const Header = () => {
    return <div style={{marginLeft: "auto"}}>
    <Link to={`/registration`}><button>Регистрация</button></Link>
    <Link to={`/login`}><button>Войти</button></Link>
    <Link to={`/logout`}><button>Выйти</button></Link>
    <Link to={`/profile`}><button>Профиль</button></Link>
    <Link to={`/feed`}><button>Лента новостей</button></Link>
    <Link to={`/`}><button>Найти нужный материал</button></Link>

</div>
}

export default Header;