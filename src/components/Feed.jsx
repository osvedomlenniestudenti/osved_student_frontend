import React, { useState, useEffect } from 'react';
import {Link, Navigate, useParams} from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';

const Feed = () => {
    const [pageNumber, setPageNumber] = useState(0);
    const [userData, setUserData] = useState([]);
    const [posts, setPosts] = useState([]);
    const [newPostTitle, setNewPostTitle] = useState('');
    const [newPostText, setNewPostText] = useState('');

    const fetchUser = async () => {
        try{
            await axios.get(
                `http://localhost:9999/user/profile`,
                {
                    headers: authHeader()
                }
            ).then(res => {
                setUserData(res.data);
                console.log("set user data")
            })
        }catch (error) {
            console.error('Error fetching userData:', error);
        }
    };

    const fetchData = async () => {
        try{
            await axios.get(
                `http://localhost:9999/post`,
                {
                    params: {
                        "pageNumber": pageNumber,
                        "task_id": 0
                    },
                    headers: authHeader()
                }
            ).then(res => {
                setPosts(res.data.content);
                console.log(res);
            })
        }catch (error) {
            console.error('Error fetching posts:', error);
        }
    };

    useEffect(() => {
        fetchData();
        fetchUser();
    }, [pageNumber]);

    const addPost = async () => {
        try {
            await axios.post(
                `http://localhost:9999/post`,
                {
                    title: newPostTitle,
                    text: newPostText,
                    taskId: 0
                },
                {
                    headers: authHeader()
                }
            ).then(res => {
                setPosts(prevPosts => [...prevPosts, res.data]);
            })
                .catch(res => {
                    if (res.response.status === 400) {
                        alert(res.response.data.message)
                    }else {
                        alert("у вас недостаточно прав")
                    }
                });
        } catch (error) {
            console.error('Error adding post:', error);
        }
    };

    const deletetPost = async(id) => {
        await axios.delete(
            `http://localhost:9999/post/` + id,
            {
                headers: authHeader()
            }
        ).then(res => {
            fetchData();
        }).catch(() => alert("у вас недостаточно прав"))
    }

    const checkAdmin = () => {
        return userData.roles !== undefined && userData.roles.indexOf("ROLE_ADMIN") > -1;
    }

    const checkModerator = () => {
        return userData.roles !== undefined && userData.roles.indexOf("ROLE_MODERATOR") > -1;
    }

    if (authHeader() === undefined) {
        return <Navigate to="/login" />;
    }

    return (
        <div>
            <h2>Новостная лента</h2>
            { (checkAdmin() || checkModerator()) &&
                <div>
                    <input
                        type="text"
                        placeholder="Заголовок"
                        value={newPostTitle}
                        onChange={(e) => setNewPostTitle(e.target.value)}
                    /><br/>
                    <textarea
                        type="text"
                        placeholder="Текст"
                        value={newPostText}
                        onChange={(e) => setNewPostText(e.target.value)}
                    /><br/>
                    <button onClick={addPost}>Добавить</button>
                </div>
            }
            <div style={{display: 'block'}}>
                {posts.map(post => (
                    <div key={post.id}>
                        <Link to={`/feed/comments/${post.id}`} key={post.id}>
                            <div style={{border: "1px solid black", width: "500px", textAlign: 'center'}}>
                                <h4>{post.title}</h4>
                                <p>Автор: {post.author.name}</p>
                                <p>Создан: {post.created_at}</p>
                                <p>{post.text}</p>
                            </div>
                        </Link>
                        <Link to={`/profile/` + post.author.name}><button> открыть профиль</button></Link>
                        {checkAdmin() &&
                            <button onClick={() => deletetPost(post.id)}>удалить</button>
                        }
                    </div>
                ))}
            </div>
            <button onClick={() => setPageNumber(Math.max(pageNumber - 1, 0))}>предыдушая страница</button>
            <div>Страница номер: {pageNumber} </div>
            <button onClick={() => setPageNumber(pageNumber + 1)}>слудующая страница</button>
        </div>
    );
};

export default Feed;
