import AuthService from "../services/AuthService";
import { Navigate } from "react-router-dom";

const Logout = () => {
    const logoutUser = async () => {
        AuthService.logout();
    };
    logoutUser();

    return <Navigate to="/" />;
};

export default Logout;