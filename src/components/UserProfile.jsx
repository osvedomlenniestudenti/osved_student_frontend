import React, { useState, useEffect } from 'react';
import authHeader from '../util/authHeader';
import axios from 'axios';
import {
    Link,
    Navigate,
    useLocation,
} from 'react-router-dom';



const UserProfile = () => {
    const [userData, setUserData] = useState()
    const [adminUserData, setAdminUserData] = useState()

    const UserProfileCard = (userData) => {
        return <div>
              
            <p> name : {userData.name}</p>
            <p> age : {userData.age}</p>
            <p> email : {userData.email}</p>
            <p> telegram : {userData.telegram}</p>
            <p> description: {userData.description}</p>
            <p> роли: {userData.roles.map((elem) => " " + elem + " ")} </p>
        </div>
    }

    const AdminProfileCard = (userData) => {
        return <div key = {userData.name}>
            <p> name : {userData.name}</p>
            <button onClick={()=> deleteUserByName(userData.name)}>delete</button>
            <button onClick={()=> setModer(userData.name)}>setModer</button>
            <Link to={`/profile/` + userData.name}><button> открыть профиль</button></Link>
        </div>
    }

    const deleteUserByName = async (name) => {
        await axios.delete(
            `http://localhost:9999/user/` + name,
            {
                headers: authHeader()
            }
        ).then(res => {
            getUserProfilesInfo();
        })

    }

    const setModer = async (name) => {
        console.log(name)
        await axios.post(
            `http://localhost:9999/admin/add/` + name,
            {
            },{
                headers: authHeader()
            }
        ).then(res => {
            getUserProfilesInfo();
        })
    }

    const fetchData = async () => {

        await axios.get(
            `http://localhost:9999/user/profile`,
            {
                headers: authHeader()
            }
        ).then(res => {
            setUserData(res.data);
        }).catch((res) => {
            //
        })

    };

    const getUserProfilesInfo = async () => {
        await axios.get(
            `http://localhost:9999/admin/profile`,
            {
                headers: authHeader()
            }
        ).then((res) => {setAdminUserData(res.data)})
            .catch((res) => {
                console.log(res);
            })
    };

    if (authHeader() === undefined) {

        return <Navigate to="/login" />;

    }

    if (userData === undefined) {
        return <div>
            <button onClick={fetchData}> загрузить данные профиля  </button>
        </div>
    }
  

    if (userData.roles !== undefined && userData.roles.indexOf("ROLE_ADMIN") > -1) {
        return <div>
            {UserProfileCard(userData)}
            <button onClick={fetchData}> загрузить данные профиля </button>
        
            {adminUserData === undefined ? '' : adminUserData.map((userData) => AdminProfileCard(userData))}
            <button onClick={() => getUserProfilesInfo()}>загрузить всех профилей</button>
        </div>
    }
    return <div>
        {UserProfileCard(userData)}
        <button onClick={fetchData}> загрузить данные профиля </button>
    </div>
}
export default UserProfile;