import React, { useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';

const Subject = () => {
  const [pageNumber, setPageNumber] = useState(0);
  const [course, setCourse] = useState(null);

  const [subjects, setSubjects] = useState([]);

  const fetchData = async () => {
    try {
      await axios.get(
        `http://localhost:9999/subject`,
        {
          params: {
            "pageNumber": pageNumber,
            "course": course
          },
          headers: authHeader()

        }
      ).then(res => {
        setSubjects(res.data.content);
      });
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [course, pageNumber]);
 
  const [name, setName] = useState('');
  const [courseCreate, setCreateCourse] = useState(1);

  const addSubject = async () => {
    try {
      await axios.post(
        `http://localhost:9999/subject`,
        {
          name: name,
          course: courseCreate
        },
        {
          headers: authHeader()
        }
      ).then(res => {
        fetchData();
      }).catch(res => {
            alert(res.response.data.message)
          
        });
    } catch (error) {
      console.error('Error adding subject:', error);
    }
  };

  const deleteSubject = async(id) => {
    await axios.delete(
      `http://localhost:9999/subject/` + id,
      {
          headers: authHeader()
      }
  ).then(res => {
    fetchData();
  }).catch(() => alert("у вас недостаточно прав"))
  }

  return (
    <div>
      <h2>Предметы</h2>
      <div>

        <input
          type="text"
          placeholder="Название предмета"
          required
          name="name"
          value={name}
          onChange={(event) => setName(event.target.value)}
        />

        <label >
          курс:
          <select name="course" required selected={courseCreate} onChange={(event) => setCreateCourse(event.target.value)}>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </label>
        <button onClick={addSubject}>Добавить</button>
      </div>

      <div style={{ display: 'block' }}>
        <h2>Выберите курс</h2>
        <nav>
          {
            [1, 2, 3, 4, 5, 6].map(elem =>
              <button key={elem} onClick={() => {
                if (elem == course)
                  setCourse(null);
                else
                  setCourse(elem);
              }
              }>
                {elem}
              </button>
            )
          }
        </nav>

        {subjects.map(subject => (
          <div key ={subject.id}>
          <Link to={`/tasks/${subject.id}`} key={subject.id}>
            <div style={{ border: "1px solid black", width: "200px", textAlign: 'center' }}>
              <p>Название: {subject.name}</p>
              <p>автор: {subject.author.name}</p>
            </div>
          </Link>
           <Link to={`/profile/` + subject.author.name}><button> открыть профиль</button></Link>
           <button onClick={() => deleteSubject(subject.id)}>удалить</button>
           </div>
        ))}
        <button onClick={() => setPageNumber(Math.max(pageNumber - 1, 0))}>предыдушая страница</button>
        <div>Страница номер: {pageNumber} </div>
        <button onClick={() => setPageNumber(pageNumber + 1)}>слудующая страница</button>
      </div>
    </div>
  );
};

export default Subject;
