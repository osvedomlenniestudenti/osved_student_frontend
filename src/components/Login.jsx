import { useState } from "react";
import AuthService from "../services/AuthService";
import { useNavigate } from "react-router-dom";

const Login = () => {
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');

    const loginUser = async () => {
        AuthService.login(name, password)
        .then(res => {
            if (res === 200) {
                navigate("/feed");
            }
        });
    };

    return (
        <div style={{width: "120px"}}>
            <p>Авторизация</p>
            <label>Введите имя</label><input type="text" value={name} onChange={(e) => setName(e.target.value)} required /><br/>
            <label>Введите пароль</label><input type="password" value={password} onChange={(e) => setPassword(e.target.value)} required /><br/>
            <button onClick={loginUser}>Войти</button>
        </div>
    );
};

export default Login;