import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';

const typesOptions = [
  {value: "TEST", text: "Контрольная работа"},
  {value: "ABSTRACT", text: "Конспект"},
  {value: "LITERATURE", text: "Литература"},
  {value: "EXAM", text: "Экзамен"}
]

const Task = () => {
  const { subjectId } = useParams();
  const [tasks, setTasks] = useState([]);
  const [newTaskTitle, setNewTaskTitle] = useState('');
  const [newTaskType, setNewTaskType] = useState(typesOptions[0].value);
  const [pageNumber, setPageNumber] = useState(0);


  const fetchData = async () => {
    try {
      await axios.get(
        `http://localhost:9999/task`,
        { 
          params: {
            "subjectId": subjectId,
            "pageNumber": pageNumber
          },
          headers: authHeader()
        }
      ).then(res => {
        setTasks(res.data.content);
      });
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [subjectId, pageNumber]);

  const addTask = async () => {

    try {
        await axios.post(
            `http://localhost:9999/task`,
            { 
                title: newTaskTitle,
                type: newTaskType,
                subjectId: parseInt(subjectId)
            },
            {
              headers: authHeader()
            }
        ).then(res => {
          fetchData();
        })
        .catch(res => {
          if (res.response.status === 400) {
            alert(res.response.data.message)
          }
        });
    } catch (error) {
        console.error('Error adding task:', error);
    }
  };

  const deleteTask = async(id) => {
    await axios.delete(
      `http://localhost:9999/task/` + id,
      {
          headers: authHeader()
      }
  ).then(res => {
    fetchData();
  }).catch(() => alert("у вас недостаточно прав"))
  }

  return (
    <div>
      <h2>Задания</h2>
      <div>
        <input
            type="text"
            placeholder="Название задания"
            value={newTaskTitle}
            onChange={(e) => setNewTaskTitle(e.target.value)}
        />
        <select value={newTaskType} onChange={(e) => setNewTaskType(e.target.value)}>
          {typesOptions.map(option => (
            <option key={option.value} value={option.value}>
              {option.text}
            </option>
          ))}
        </select>
        <button onClick={addTask}>Добавить</button>
      </div>
      <div style={{display: 'block'}}>
        {tasks.map(task => (
           <div key={task.id}>
          <Link to={`/posts/${task.id}`} key={task.id}>
            <div style={{border: "1px solid black", width: "300px", textAlign: 'center'}}>
              <h4>{task.title}</h4>
              <p>Тип: {task.type}</p>
              <p>Автор: {task.author.name}</p>
              <p>Создан: {task.created_at}</p>
            </div>
          </Link>
          <Link to={`/profile/` + task.author.name}><button> открыть профиль</button></Link>
          <button onClick={() => deleteTask(task.id)}>удалить</button>
          </div>
        ))}
      </div>
      <button onClick={() => setPageNumber(Math.max(pageNumber - 1, 0))}>предыдушая страница</button>
        <div>Страница номер: {pageNumber} </div>
      <button onClick={() => setPageNumber(pageNumber + 1)}>слудующая страница</button>
    </div>
  );
};

export default Task;
