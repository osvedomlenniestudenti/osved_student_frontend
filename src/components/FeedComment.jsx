import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';
import { Link } from 'react-router-dom';


const FeedComment = () => {
  const { postId } = useParams();
  const [isChanging, setIsChanging] = useState(false);
  const [post, setPost] = useState({});
  const [comments, setComments] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [anonymous, setAnonymous] = useState();

  const [newCommentText, setNewCommentText] = useState('');
  const [updateCommentText, setUpdateCommentText] = useState('');
    const [idChanging, setIdChanging] = useState(-1);

  const fetchData = async () => {
    try {
      await axios.get(
        `http://localhost:9999/comment`,
        {
          params: {
            "pageNumber": pageNumber,
            "postId": postId,
            "parentId":null
          },
          headers: authHeader()
        }
      ).then(res => {
        setComments(res.data.content);
      });
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const fetchPost = async () => {
    try {
      await axios.get(
        `http://localhost:9999/post/${postId}`,
        {
          headers: authHeader()
        }
      ).then(res => {
        setPost(res.data);
      });
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchPost();
    fetchData();
  }, [postId, pageNumber]);

  const addComment = async () => {
    try {
        await axios.post(
            `http://localhost:9999/comment`,
            { 
                text: newCommentText,
                postId: parseInt(postId),
                anonymous: anonymous
            },
            {
              headers: authHeader()
            }
        ).then(res => {
          fetchData();
        })
        .catch(res => {
          if (res.response.status === 400) {
            alert(res.response.data.message)
          }
        });
    } catch (error) {
        console.error('Error adding post:', error);
    }
  };

  const updateComment = async(id) => {
    if (!isChanging) {
      setIsChanging(true)
        setIdChanging(id)
      return 
    }

    await axios.put(
        `http://localhost:9999/comment/` + id,
        { 
          text: updateCommentText
        },
        {
            headers: authHeader()
        }
    ).then(res => {
      fetchData();
    }).catch(() => alert("у вас недостаточно прав"))

    setIsChanging(false);
      setIdChanging(-1);
      setUpdateCommentText('');
  }

  const deletetComment = async(id) => {
    await axios.delete(
        `http://localhost:9999/comment/` + id,
        {
            headers: authHeader()
        }
    ).then(res => {
      fetchData();
    }).catch(() => alert("у вас недостаточно прав"))
  }

  return (
    <div>
      <div style={{border: "1px solid black", width: "500px", textAlign: 'center'}}>
        <h4>{post.title}</h4>
        <p>Создан: {post.created_at}</p>
        <p>{post.text}</p>
      </div>
      <h2>Комментарии</h2>
      <div>
        <textarea
            type="text"
            placeholder="Текст"
            value={newCommentText}
            onChange={(e) => setNewCommentText(e.target.value)}
        /><br/>
         <label>
         анонимно: <input type="checkbox" name="myCheckbox" value={anonymous} onClick={() => setAnonymous(!anonymous)}/>
        </label>
        <button onClick={addComment}>Добавить</button>
      </div>
      <div style={{display: 'block', marginBottom: "30px"}}>
        {comments.map(comment => (
          <div key ={comment.id}>
              <Link to={`/feed/comments/${post.id}/${comment.id}`} key={comment.id}>
                  <div style={{border: "1px solid black", width: "200px", textAlign: 'center'}}>
                    <p>Автор: {comment.author.name}</p>
                    <p>Создан: {comment.created_at}</p>
                    {comment.created_at != comment.updated_at &&
                      <p>Изменено</p>
                    }
                    <p>{comment.text}</p>
                  </div>
              </Link>
              {
                  isChanging && comment.id === idChanging  &&
                <div>
                  <textarea
                      type="text"
                      placeholder="Текст"
                      value={updateCommentText}
                      onChange={(e) => setUpdateCommentText(e.target.value)}
                  />
                </div>
              }
          <button onClick={() => updateComment(comment.id)}>изменить</button>
          <button onClick={() => deletetComment(comment.id)}>удалить</button>
          <br/>
          {!comment.anonymous &&
            <Link to={`/profile/` + comment.author.name}><button> открыть профиль</button></Link>
          }
          </div>
        ))}
      </div>
      <button onClick={() => setPageNumber(Math.max(pageNumber - 1, 0))}>предыдушая страница</button>
        <div>Страница номер: {pageNumber} </div>
      <button onClick={() => setPageNumber(pageNumber + 1)}>следующая страница</button>
    </div>
  );
};

export default FeedComment;
