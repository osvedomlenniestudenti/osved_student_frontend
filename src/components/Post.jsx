import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';

const Post = () => {
  const { taskId } = useParams();
  const [pageNumber, setPageNumber] = useState(0);

  const [posts, setPosts] = useState([]);
  const [newPostTitle, setNewPostTitle] = useState('');
  const [newPostText, setNewPostText] = useState('');

  const fetchData = async () => {
    try {
      await axios.get(
        `http://localhost:9999/post`,
        {
          params: {
            "pageNumber": pageNumber,
            "taskId": taskId
          },
          headers: authHeader()
        }
      ).then(res => {
        setPosts(res.data.content);
        console.log(res);
      });
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [taskId, pageNumber]);

  const addPost = async () => {
    try {
        await axios.post(
            `http://localhost:9999/post`,
            { 
                title: newPostTitle,
                text: newPostText,
                taskId: parseInt(taskId)
            },
            {
              headers: authHeader()
            }
        ).then(res => {
          setPosts(prevPosts => [...prevPosts, res.data]);
        })
        .catch(res => {
          if (res.response.status === 400) {
            alert(res.response.data.message)
          }
        });
    } catch (error) {
        console.error('Error adding post:', error);
    }
  };

  const deletetPost = async(id) => {
    await axios.delete(
      `http://localhost:9999/post/` + id,
      {
          headers: authHeader()
      }
  ).then(res => {
    fetchData();
  }).catch(() => alert("у вас недостаточно прав"))
  }
  return (
    <div>
      <h2>Посты</h2>
      <div>
        <input
            type="text"
            placeholder="Заголовок"
            value={newPostTitle}
            onChange={(e) => setNewPostTitle(e.target.value)}
        /><br/>
        <textarea
            type="text"
            placeholder="Текст"
            value={newPostText}
            onChange={(e) => setNewPostText(e.target.value)}
        /><br/>
          <button onClick={addPost}>Добавить</button>
          <input type="file" />
      </div>
      <div style={{display: 'block'}}>
        {posts.map(post => (
          <div key={post.id}>
          <Link to={`/comments/${post.id}`} key={post.id}>
            <div style={{border: "1px solid black", width: "500px", textAlign: 'center'}}>
              <h4>{post.title}</h4>
              <p>Автор: {post.author.name}</p>
              <p>Создан: {post.created_at}</p>
              <p>{post.text}</p>
            </div>
          </Link>
          <Link to={`/profile/` + post.author.name}><button> открыть профиль</button></Link>
          <button onClick={() => deletetPost(post.id)}>удалить</button>
          </div>
        ))}
      </div>
      <button onClick={() => setPageNumber(Math.max(pageNumber - 1, 0))}>предыдушая страница</button>
        <div>Страница номер: {pageNumber} </div>
      <button onClick={() => setPageNumber(pageNumber + 1)}>слудующая страница</button>
    </div>
  );
};

export default Post;
