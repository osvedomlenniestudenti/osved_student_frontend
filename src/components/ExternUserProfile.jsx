import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';


const ExternUserProfile = () => {
    const  {username} = useParams();
    const [userData, setUserData] = useState()

    const getUserData = async (name) => {
        await axios.get(
            `http://localhost:9999/user/info/` + username,
            {
                headers: authHeader()
            }
        ).then(res => {
            setUserData(res.data);
        }).catch((res) => {
            //
        })
    }

    const UserProfileCard = (userData) => {
        return <div>
            <p> name : {userData.name}</p>
            <p> age : {userData.age}</p>
            <p> email : {userData.email}</p>
            <p> telegram : {userData.telegram}</p>
            <p> description: {userData.description}</p>
            <p> роли: {userData.roles} </p>
        </div>
    }

    if (userData === undefined) 
        return <button onClick={getUserData}> загрузить данные профиля </button>;
    return <div>
        {UserProfileCard(userData)}
        <button onClick={getUserData}> загрузить данные профиля </button>
    </div>

}

export default ExternUserProfile;