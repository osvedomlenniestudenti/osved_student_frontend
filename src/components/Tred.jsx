import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import authHeader from '../util/authHeader';
import { Link } from 'react-router-dom';


const Comment = () => {
    const { postId } = useParams();
    const { commentId } = useParams();
    const [comment, setComment] = useState({});
    const [comments, setComments] = useState([]);
    const [pageNumber, setPageNumber] = useState(0);
    const [anonymous, setAnonymous] = useState();

    const [newCommentText, setNewCommentText] = useState('');
    const [updateCommentText, setUpdateCommentText] = useState('');
    const [isChanging, setIsChanging] = useState(false);
    const [idChanging, setIdChanging] = useState(-1);

    const fetchData = async () => {
        try {
            await axios.get(
                `http://localhost:9999/comment`,
                {
                    params: {
                        "pageNumber": pageNumber,
                        "postId": postId,
                        "parentId": commentId
                    },
                    headers: authHeader()
                }
            ).then(res => {
                setComments(res.data.content);
            });
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const fetchComment = async () => {
        try {
            await axios.get(
                `http://localhost:9999/comment/${commentId}`,
                {
                    headers: authHeader()
                }
            ).then(res => {
                setComment(res.data);
            });
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchComment();
        fetchData();
    }, [commentId, pageNumber]);

    const updateComment = async(id) => {
        if (!isChanging) {
            setIsChanging(true)
            setIdChanging(id)
            return
        }

        await axios.put(
            `http://localhost:9999/comment/` + id,
            {
                text: updateCommentText
            },
            {
                headers: authHeader()
            }
        ).then(res => {
            fetchData();
        }).catch(() => alert("у вас недостаточно прав"))

        setIsChanging(false);
        setIdChanging(-1);
        setUpdateCommentText('')
    }

    const addComment = async () => {
        try {
            await axios.post(
                `http://localhost:9999/comment`,
                {
                    text: newCommentText,
                    postId: parseInt(postId),
                    parentId: parseInt(commentId),
                    anonymous: anonymous
                },
                {
                    headers: authHeader()
                }
            ).then(res => {
                fetchData();
            })
            .catch(res => {
                if (res.response.status === 400) {
                    alert(res.response.data.message)
                }
            });
        } catch (error) {
            console.error('Error adding post:', error);
        }
    };

    const deletetComment = async(id) => {
        await axios.delete(
            `http://localhost:9999/comment/` + id,
            {
                headers: authHeader()
            }
        ).then(res => {
            fetchData();
        }).catch(() => alert("у вас недостаточно прав"))
    }
    return (
        <div>
            <div style={{border: "1px solid black", width: "500px", textAlign: 'center'}}>
                <p>Создан: {comment.created_at}</p>
                <p>{comment.text}</p>
            </div>
            <h2>Тред</h2>
            <div>
        <textarea
            type="text"
            placeholder="Текст"
            value={newCommentText}
            onChange={(e) => setNewCommentText(e.target.value)}
        /><br/>
                <label>
                    анонимно: <input type="checkbox" name="myCheckbox" value={anonymous} onClick={() => setAnonymous(!anonymous)}/>
                </label>
                <button onClick={addComment}>Добавить</button>
            </div>
            <div style={{display: 'block', marginBottom: "30px"}}>
                {comments.map(comment => (
                    <div key ={comment.id}>
                        <div style={{border: "1px solid black", width: "200px", textAlign: 'center'}}>
                            <p>Автор: {comment.author.name}</p>
                            <p>Создан: {comment.created_at}</p>
                            {comment.created_at != comment.updated_at &&
                                <p>Изменено</p>
                            }
                            <p>{comment.text}</p>
                        </div>
                        {!comment.anonymous &&
                            <Link to={`/profile/` + comment.author.name}><button> открыть профиль</button></Link>
                        }
                        {
                            isChanging && comment.id === idChanging  &&
                            <div>
                  <textarea
                      type="text"
                      placeholder="Текст"
                      value={updateCommentText}
                      onChange={(e) => setUpdateCommentText(e.target.value)}
                  />
                            </div>
                        }
                        <button onClick={() => updateComment(comment.id)}>изменить</button>
                        <button onClick={() => deletetComment(comment.id)}>удалить</button>
                    </div>
                ))}
            </div>
            <button onClick={() => setPageNumber(Math.max(pageNumber - 1, 0))}>предыдушая страница</button>
            <div>Страница номер: {pageNumber} </div>
            <button onClick={() => setPageNumber(pageNumber + 1)}>следующая страница</button>
        </div>
    );
};

export default Comment;
